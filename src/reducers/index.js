import { combineReducers } from 'redux';

import {
    listOfContacts,
    displayAddNewContactScreen
} from './reducers';

const rootReducer = combineReducers({
    listOfContacts,
    displayAddNewContactScreen
});

export default rootReducer;
