import {
    ADD_NEW_CONTACT,
    REPORT_LOAD_CONTACTS_FROM_ADDRESS_BOOK,
    REPORT_DISPLAY_ADD_NEW_CONTACT_SCREEN
} from '../actions/types';

export const listOfContacts = (state = [], action) => {
    switch(action.type) {
        case REPORT_LOAD_CONTACTS_FROM_ADDRESS_BOOK:
            return action.payload;
        case ADD_NEW_CONTACT:
            let stateClone = [...state];
            stateClone.push(action.payload);
            return stateClone;
    }
    return state;
};

export const displayAddNewContactScreen = (state=false,action) => {
    switch (action.type) {
        case REPORT_DISPLAY_ADD_NEW_CONTACT_SCREEN:
            return action.payload;
    }
    return state;
};