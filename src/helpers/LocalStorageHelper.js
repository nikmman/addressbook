export const getAddressBookFromLocalStorage = () => {
    let url = window.location.href+"_addressBook";
    return window.localStorage.getItem(url);
};

export const setAddressBookInLocalStorage = (data) => {
    let url = window.location.href+"_addressBook";
    window.localStorage.setItem(`${url}`,JSON.stringify(data));
};