import {
    REPORT_LOAD_CONTACTS_FROM_ADDRESS_BOOK,
    ADD_NEW_CONTACT,
    REPORT_DISPLAY_ADD_NEW_CONTACT_SCREEN
} from './types';

export const reportLoadContactsFromAddressBook = (contacts) => {
    return {
        type: REPORT_LOAD_CONTACTS_FROM_ADDRESS_BOOK,
        payload: contacts
    }
};

export const addNewContact = (contact) => {
    return {
        type: ADD_NEW_CONTACT,
        payload: contact
    }
};

export const reportDisplayAddNewContactScreen = (trueOrFalse) => {
    return {
        type: REPORT_DISPLAY_ADD_NEW_CONTACT_SCREEN,
        payload: trueOrFalse
    }
};

