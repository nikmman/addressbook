import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {reportDisplayAddNewContactScreen} from '../actions';

export class FloatingButton extends Component {
    render() {
        return (
            <div className="floating-btn-container" onClick={e=>this.props.reportDisplayAddNewContactScreen(true)}>
                <div className="floating-button-icon">+</div>
                <div>Add New Contact</div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        reportDisplayAddNewContactScreen
    },dispatch);
};

export default connect(null,mapDispatchToProps)(FloatingButton);
