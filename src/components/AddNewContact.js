import React, { Component } from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {reportDisplayAddNewContactScreen} from "../actions";

export class AddNewContact extends Component {
    render() {
        return (
            <div className="add-new-contact-screen" onClick={e=>this.props.reportDisplayAddNewContactScreen(false)}>
                <div className="add-new-contact-container" onClick={(e)=>{e.stopPropagation()}}>

                    <div className="add-new-contact-header">New Contact</div>

                    <input placeholder="Enter name" className="add-new-contact-input"/>
                    <div className="add-new-contact-label">Name</div>

                    <input placeholder="Enter phone number" className="add-new-contact-input"/>
                    <div className="add-new-contact-label">Phone Number</div>

                    <input placeholder="Enter email" className="add-new-contact-input"/>
                    <div className="add-new-contact-label">Email</div>

                    <textarea placeholder="Enter notes here" className="add-new-contact-input"></textarea>
                    <div className="add-new-contact-label">Notes</div>

                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        reportDisplayAddNewContactScreen
    },dispatch);
};

export default connect(null,mapDispatchToProps)(AddNewContact);
