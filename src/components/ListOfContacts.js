import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectListOfContacts} from "../selectors/selectors";
import ContactCard from './ContactCard';

export class ListOfContacts extends Component {

    renderContacts() {
        if(this.props.listOfContacts.length > 0) {
            return this.props.listOfContacts.map((data,i)=> {
               return <ContactCard key={i}
                                   data={data}/>
            });
        }else {
            return(
                <div className="no-contacts">No Contacts Added</div>
            )
        }
    }

    render() {
        return (
            <div className="list-of-contacts">
                <div className="list-of-contacts-heading">Contacts</div>
                <div className="list-of-contacts-container">
                    {this.renderContacts()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        listOfContacts: selectListOfContacts(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

    },dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(ListOfContacts);
