import React, { Component } from 'react';

export default class ContactCard extends Component {
    render() {
        return (
            <div className="contact-card-container">
                <div style={{width: "25%"}}>{this.props.data.name}</div>
                <div style={{width: "22%"}}>{this.props.data.phoneNo}</div>
                <div style={{width: "25%"}}>{this.props.data.email}</div>
                <div style={{width: "25%"}}>{this.props.data.notes}</div>
                <div style={{width: "3%"}} className="contact-edit-btn">Edit</div>
            </div>
        );
    }
}
