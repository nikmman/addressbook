import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import FloatingButton from './FloatingButton';
import ListOfContacts from './ListOfContacts';
import AddNewContact from './AddNewContact';
import {getAddressBookFromLocalStorage,setAddressBookInLocalStorage} from '../helpers/LocalStorageHelper';
import {reportLoadContactsFromAddressBook} from '../actions';
import {selectDisplayAddNewContactScreen, selectListOfContacts} from '../selectors/selectors';


// use this to intialize address book
// (intially add two contact manually because add new contact functionality is remaining)

// contactId = new Date().getTime()+"_name"
// is to uniquely identify contact for edit contact functionality

// let addressBook = {
//     contacts: [
//         {
//             contactId: "1234355655nikunj",
//             name: "nikunj",
//             phoneNo: 9737519600,
//             email: "nikunjmangukiya0@gmail.com",
//             notes: "starred"
//         },
//         {
//             contactId: "23343324655person",
//             name: "person",
//             phoneNo: 2345678910,
//             email: "person0@gmail.com",
//             notes: "starred"
//         }
//     ]
// };
//
// setAddressBookInLocalStorage(addressBook);

export class App extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let addressBookString = getAddressBookFromLocalStorage();

        if(addressBookString) {
            let addressBook = JSON.parse(addressBookString);
            this.props.reportLoadContactsFromAddressBook(addressBook.contacts);
        }
    }

    render() {
        return (
            <div className="app-container">
                <ListOfContacts/>
                <FloatingButton/>
                {(this.props.displayAddNewContactScreen)?<AddNewContact />:false}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        displayAddNewContactScreen: selectDisplayAddNewContactScreen(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        reportLoadContactsFromAddressBook
    },dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(App);
